# DevOps Engineer

## Introduction

Hello and thank you for considering the DevOps Engineer position at [Quandoo](https://www.quandoo.com/careers/)! To assess your Cloud-Native platform design skills and determine your suitability for the role, we have prepared a task for you to complete.

To complete this task, you require a full-stack web application. You are free to choose any of the implementations of [Real World](https://github.com/gothinkster/realworld) applications that you feel most comfortable with. Be sure to select the technology that you are most proficient in. Note that the application you choose will not be subject to evaluation.

## Task description

How do you design a high-available, scalable, secure and cost-efficient platform on AWS for continuously building, testing and deploying [Real World](https://github.com/gothinkster/realworld) application to support millions of concurrent users?

Please ensure the following considerations are taken into account:

-  The deployment of new code and the execution of tests should be fully automated.
-  All relevant logs for all tiers should be easily accessible, and cannot simply be stored on the hosts.
-  The platform should retain historical metrics to detect and resolve performance bottlenecks.
-  The platform should be recovered from disasters with minimal/without any data loss within the shortest possible time.
-  Software engineers should have a streamlined and user-friendly process to deploy new applications into the platform with minimal reliance on DevOps Engineers.

## Evaluation Criteria

The primary evaluation criteria focus on ensuring that your designed platform aligns with the requirements stated above. Additionally, we will assess the following aspects:

- Tool Selection: We will evaluate the tools you have chosen to implement in your platform.
- Proper Utilization of Tools: We will assess how effectively you have utilized the selected tools to address the requirements.
- Solution Flexibility & Extensibility: We will consider the degree to which your solution can adapt to changing needs and easily accommodate future expansions or improvements.

## What to commit?

To complete the evaluation, please submit the following:

-  Provide a comprehensive platform design document, preferably written in markdown format.
-  Include relevant diagrams, created using tools like [draw.io](https://app.diagrams.net/) or similar, to illustrate your design. Include as many diagrams as necessary in the design document.
-  There is no need to write actual code at this stage, but be prepared to explain your design during the interview process.

## Submitting your solution

**Please do not spend more than a few hours on this task**

-  Fork this repo with [Visibility level](https://docs.gitlab.com/ee/user/public_access.html) set to **Private**.
-  Commit your artifacts (e.g. document, diagrams) into the private repository in `feat/quandoo` branch. We encourage you to commit often so that we can see a history of trial and error rather than a single monolithic push.
-  Share the project with the gitlab users **ReeSilva_Quandoo**, **musaib.khan1** and **maateen** (go to **Project information -> Members -> Invite member**, find the user in **Select members to invite** and set **Choose a role permission** to **Developer**).
-  Create a merge request from `feat/quandoo` branch to `main` branch in the private repository and request review from **maateen**.